from django.db import models
from audioquarium.music.models import Song, Blog, Favorite
from django.contrib.auth.models import User

class FacebookUser(models.Model):
	fb_id = models.CharField(max_length=50, unique=True)
	access_token = models.CharField(max_length=255)
	contrib_user = models.OneToOneField(User)

class AudioquariumUser(models.Model):
	#Connect to a django contrib user
	user = models.ForeignKey(User, unique=True)

	# inherits from Django contrib user
	user_influences = models.ManyToManyField(User, related_name='user_influences')	#People that this user follows
	blog_influences = models.ManyToManyField(Blog)	

#Automatically create a user profile when the profile is referenced
User.profile = property(lambda u: AudioquariumUser.objects.get_or_create(user=u)[0])
