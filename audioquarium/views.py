from django.http import HttpResponse, Http404, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.core import serializers
from django.core.paginator import Paginator
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from django.template import loader, Context
import hashlib
import datetime

from audioquarium.audioquariumusers.models import FacebookUser
from audioquarium.music.models import Blog, Song, Favorite

THUMBNAIL_SONGS_PER_PAGE = 8

def home(request):
	title = 'Incoming Music'

	try:
		fbUser = FacebookUser.objects.get(contrib_user=request.user.id)
		return render_to_response('music_page.html', {'page_title': title, 'user': request.user, 'fb_user': fbUser}) 
	except FacebookUser.DoesNotExist:
		return render_to_response('music_page.html', {'page_title': title, 'user': request.user})

def redirect_to_hash_page(request, page):
	return HttpResponseRedirect('/#incoming/' + page)

def pop_favorites_data(user,songs):
	#
	# Summary:
	# 	Show richer data (favorited? favorited by a friend? posted by someone we are following?)
	#
	# user: 
	#	The logged in user.
	#
	# songs:
	#	The posts to populate with extra favorite data. The same object is returned with the extra
	# 	fields.

	for song in songs:
		try:
			fave = Favorite.objects.get(user=user,song=song)
			song.favorite = fave.time_favorited # if song.favorite is a date: favorited by the logged-in user
		except Favorite.DoesNotExist:
			song.favorite = 0 # song has not been favorited by user or user's network

		# See what friends (musical influences) favorited it
		try:
			influences = user.profile.user_influences
			songs.favorites_by_friends = Favorite.objects.filter(user__in=influences,song=song)
		except:
			#no favorites by friends for this song
			pass	
	return songs

#Ajax views
def get_posts_chronologically(request, page):
	try:
		page = int(page)
	except ValueError:
		raise Http404()
	
	necessary_fields = ('title', 'artist', 'media_source', 'blog_sources', 'time_posted')
	incoming_songs = Song.objects.order_by('-time_posted')
	incoming_songs = Paginator(incoming_songs, THUMBNAIL_SONGS_PER_PAGE)

	if page > incoming_songs.num_pages:
		raise Http404()
	songs_for_page = incoming_songs.page(page).object_list
	
	# If the user is logged in apply favorites data to the music
	if request.user.is_authenticated():
		songs_for_page = pop_favorites_data(request.user, songs_for_page)

	serialized_songs = serializers.serialize('json', songs_for_page, fields=necessary_fields, extras=('favorite','favorites_by_friends'), use_natural_keys=True)
	
	return HttpResponse(serialized_songs, mimetype='application/json')

def get_favorites(request, username, page):
	try:
		page = int(page)
	except ValueError:
		raise Http404()

	try:
		user = User.objects.get(username=username)
		favorites = Favorite.objects.filter(user=user).order_by('-time_favorited')

		favorite_songs = []
		for favorite in favorites:
			favorite_songs.append(favorite.song)

		
		necessary_fields = ('title', 'artist', 'media_source', 'blog_sources', 'time_posted')
		favorite_songs = Paginator(favorite_songs, THUMBNAIL_SONGS_PER_PAGE)

		if page > favorite_songs.num_pages:
			raise Http404()
		songs_for_page = favorite_songs.page(page).object_list
		
		# If the user is logged in apply favorites data to the music
		if request.user.is_authenticated():
			songs_for_page = pop_favorites_data(request.user, songs_for_page)

		serialized_songs = serializers.serialize('json', songs_for_page, fields=necessary_fields, extras=('favorite','favorites_by_friends'), use_natural_keys=True)
		
		return HttpResponse(serialized_songs, mimetype='application/json')
			
	except User.DoesNotExist:
		raise Http404()

def get_num_section_pages(request, section):
	if section == 'incoming':
		pages = Paginator(Song.objects.all(), THUMBNAIL_SONGS_PER_PAGE).num_pages
	elif section == 'search':
		if 'q' in request.GET:
			q = request.GET['q']
			results = cg_search_music(q)
			pages = Paginator(results, THUMBNAIL_SONGS_PER_PAGE).num_pages
		else:
			raise Http404()
	elif section == 'favorites':
		if 'user' in request.GET:		
			user = User.objects.get(username=request.GET['user'])
			favorites = Favorite.objects.filter(user=user)

			favorite_songs = []
			for favorite in favorites:
				favorite_songs.append(favorite.song)

			pages = Paginator(favorite_songs, THUMBNAIL_SONGS_PER_PAGE).num_pages
		else:
			raise Http404()
	else:
		raise Http404()
	
	return HttpResponse(pages, mimetype='application/json')		

def get_next_song(request, section):
	if section == 'incoming':
		try:
			current_song_pk = int(request.GET['q'])
		except ValueError:
			raise Http404()
		
		songs = Song.objects.order_by('-time_posted')
		next_song = None
		
		for i in range(len(songs)):
			if songs[i].pk == current_song_pk:
				try:
					next_song = [songs[i+1]]	#must be iterable
				except:
					raise Http404()
		
		if not next_song:
			raise Http404()
		else:
			necessary_fields = ('title', 'artist', 'media_source', 'blog_sources', 'time_posted')
			serialized_song = serializers.serialize('json', next_song, fields=necessary_fields, use_natural_keys=True)
		
			return HttpResponse(serialized_song, mimetype='application/json')
	raise Http404()

def cg_search_music(q):
	# Content generator for search_music
	from itertools import chain
	results = list(chain(Song.objects.filter(title__icontains=q), \
			Song.objects.filter(artist__icontains=q).exclude(title__icontains=q)))

	return results

def search_music(request, page):
	if 'q' in request.GET:
		q = request.GET['q']
		
		try:
			page = int(page)
		except ValueError:
			raise Http404()
		
		results = cg_search_music(q)

		results = Paginator(results, THUMBNAIL_SONGS_PER_PAGE)
		if page > results.num_pages:
			raise Http404()
		paged_results = results.page(page).object_list
		
		necessary_fields = ('title', 'artist', 'media_source', 'blog_sources', 'time_posted')
		serialized_songs = serializers.serialize('json', paged_results, fields=necessary_fields, use_natural_keys=True)
	
		return HttpResponse(serialized_songs, mimetype='application/json')
	raise Http404()


def login(request):
	# TODO: Fix Security Vulnerability--
	# Can login with FB using only the fbID, fix to use access_token as a password
	if request.method == 'POST':
		# Look for username or fbId (one or the other will be POSTed)	
		username = request.POST.get('username', False)
		fbId = request.POST.get('fbId', False)
		
		# For debugging
		#return HttpResponse('username: %s, fbID: %s' % (username, fbId))
		
		#
		#standard user login
		#
		if username:
			password = request.POST.get('password', '')
		#
		#fb user login
		#
		elif fbId:
			
			#Check if this user exists in our system
			try:
				fb_user = FacebookUser.objects.get(fb_id=fbId)
			#If not make a new user
			except FacebookUser.DoesNotExist:
				
				# create a random hash code for the fb users contrib password	
				#temp = hashlib.new('sha1')
				#temp.update(str(datetime.datetime.now()))
				#randHash = temp.hexdigest()[:29]
				
				# password is the first 29 characters of the initial facebook access_token
				accessToken = request.POST.get('accessToken','')

				contrib_user = User()
				contrib_user.save()
				contrib_user.username = u"fbuser_%s" % contrib_user.id
				contrib_user.set_password(accessToken[:29])
				contrib_user.save()
								
				# FB User specific fields
				fb_user = FacebookUser()
				fb_user.fb_id = fbId
				fb_user.access_token = accessToken
				fb_user.contrib_user = contrib_user
				fb_user.save()
			
			username = fb_user.contrib_user
			password = fb_user.access_token[:29]
			
			#update the the following everytime they login via facebook
			contrib_user = User.objects.get(username=fb_user.contrib_user)
			contrib_user.email = request.POST.get('email', '')
			contrib_user.save()
		# No fb id or username
		else:
			return HttpResponse('0')
			
		# Log the user in whether it be an fb user or regular user
		user = auth.authenticate(username=username, password=password)
		if user is not None and user.is_active:
			# Correct password, and the user is marked as "active"
			auth.login(request, user)
			# 
			return HttpResponse(user.username)
		else:
			# Return error
			return HttpResponse('0')
				
	return HttpResponse('0')
	
def logout(request):
	auth.logout(request)
	if getattr(request, 'facebook', False):
		request.facebook.session_key = None
		request.facebook.uid = None
	return HttpResponse('1')

def toggle_favorite(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			try:	
				selected_post = Song.objects.get(pk=request.POST.get('q',''))
				try:
					#See if user has in favorites already
					fave = Favorite.objects.get(user=request.user,song=selected_post)

					#User already has this in his/her favorites
					fave.delete()
					return HttpResponse('-1')
				except Favorite.DoesNotExist:
					#User does not have this song in his/her favorites
					fave = Favorite()
					fave.user = request.user
					fave.song = selected_post
					fave.save()
					
					return HttpResponse('1')	
				#finally:	
				#Always do this even if favoriting fails
				#return HttpResponse('0')
			except Song.DoesNotExist:
				raise Http404()
			
		else:
			return HttpResponseForbidden()	
	else:
		raise Http404()


#if debug mode is on
def debug(request):
	return HttpResponse('Debug page.')

def base(request):
	return render_to_response('base.html')

def show_color(request):
	if "favorite_color" in request.COOKIES:
		
		return HttpResponse("Your favorite color is: %s" % request.COOKIES['favorite_color'])
	else:
		return HttpResponse("You don't have a favorite color.")
	
def set_color(request):
	if 'favorite_color' in request.GET:
		
		response = HttpResponse('Your favorite color is now: %s' % request.GET['favorite_color'])
		
		response.set_cookie('favorite_color', request.GET['favorite_color'])
		
		return response
	else:
		return HttpResponse("You didn't give a favorite color.")
	
def set_color2(request):
	if 'favorite_color' in request.GET:
		
		fav = request.GET['favorite_color']
		
		request.session['favorite_color'] = fav
		response = HttpResponse('Your favorite color is now: %s' % fav)
		
		response = HttpResponse('Your favorite color is now: %s' % request.GET['favorite_color'])
		
		response.set_cookie('favorite_color', request.GET['favorite_color'])
		
	else:
		return HttpResponse("You didn't give a favorite color.")
	
