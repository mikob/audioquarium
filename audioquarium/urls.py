from django.conf import settings
from django.conf.urls.defaults import *
from audioquarium import views

# Enable Admin
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# Example:
	# (r'^audioquarium/', include('audioquarium.foo.urls')),

	# Enable admin documentation:
	(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Enable the admin:
	(r'^admin/', include(admin.site.urls)),
	
	# Static files path (images, js, css)
	(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        	{'document_root': settings.STATIC_DOC_ROOT}),

)

urlpatterns += patterns('audioquarium.views',
	(r'^$', 'home'),
	
	#if the user puts in a page that looks similar to the incoming music hash, without the hash symbol
	(r'^incoming/(\d{1,2})/$', 'redirect_to_hash_page')
	
)

#Ajax patterns
urlpatterns += patterns('audioquarium.views',
	(r'^music/incoming/(\d{1,2})/$', 'get_posts_chronologically'),
	(r'^music/search/(\d{1,2})/$', 'search_music'),
	(r'^music/([a-z]{1,10})/pages/$', 'get_num_section_pages'), #between 1-10 char section
	(r'^music/([a-z]{1,10})/next_song/$', 'get_next_song'),
	(r'^user/(.{1,30})/favorites/(\d{1,3})/$', 'get_favorites'),
	(r'^account/login/$', 'login'),
	(r'^account/logout/$', 'logout'),
)

#User stuff
urlpatterns += patterns('',
	(r'^account/toggle_favorite/$', 'audioquarium.views.toggle_favorite'),
)

if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^debuginfo/$', views.debug),
		(r'^base/$', views.base),
		(r'^color/$', views.show_color),
		(r'^set_color/$', views.set_color),
		(r'^set_color2/$', views.set_color2)
	)

