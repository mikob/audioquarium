from django.db import models
from django.contrib.auth.models import User

class BlogManager(models.Manager):
	def get_by_natural_key(self, name):
		return self.get(name=name)

class Blog(models.Model):
	objects = BlogManager()
	
	# Names must be unique so we can refer to blogs by their names
	# instead of dealing with PKs 
	name = models.CharField(max_length=40, unique=True) 			
	link = models.URLField()
	feed_source = models.URLField(help_text="Include trailing slash")
	
	# Keep track of the last scrape so we can see what songs are actually new
	# last scrape is only updated when a song is actually downloaded
	last_scrape = models.DateTimeField(blank=True, editable=False, null=True)
	
	FEED_TYPES = (
		('ATOM', 'Atom'),
		('RSS', 'RSS')
	)
	feed_type = models.CharField(max_length=5, choices=FEED_TYPES)
	
	def natural_key(self):
		return (self.name)

	def __unicode__(self):
		return self.name

class Song(models.Model):
	title = models.CharField(max_length=80)
	artist = models.CharField(max_length=80)
	media_source = models.URLField()				# where the music file originally resided
	cached_source = models.CharField(max_length=40, blank=True) # name + extension of cached file (not required)
	blog_sources = models.ManyToManyField(Blog) 	# all the places this has been posted
	time_posted = models.DateTimeField(auto_now=True, editable=True, # when was it was most recently posted 
		help_text="this is automatically set to the last updated time")		
	use_cache = models.BooleanField()

	def __unicode__(self):
		return u'%s - %s' % (self.title, self.artist)

class Favorite(models.Model):
	user = models.ForeignKey(User)
	song = models.ForeignKey(Song)
	time_favorited = models.DateTimeField(auto_now=True, editable=True, 
		help_text="this is automatically se to the last updated time")	

	



