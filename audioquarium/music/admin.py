from django.contrib import admin
from audioquarium.music.models import Song, Blog

class BlogAdmin(admin.ModelAdmin):
	list_display = ('name', 'link', 'feed_type')
	list_filter = ('feed_type',)
	search_fields = ('name',)
	
class SongAdmin(admin.ModelAdmin):
	list_display = ('title', 'artist', 'time_posted')
	list_filter = ('blog_sources',)
	search_fields = ('title', 'artist', 'blog_sources')
	
admin.site.register(Blog, BlogAdmin)
admin.site.register(Song, SongAdmin)
