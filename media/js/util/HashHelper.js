(function(){
dojo.provide('audioquarium.util.HashHelper');

dojo.require('dojo.hash');

dojo.declare('audioquarium.util.HashHelper', [], {	
	
	PROPERTY_DELIMITER: '?',
	KEY_VALUE_DELIMITER: '=',
	STATE_DELIMITER: '/',
	
	// a standard state object has these properties
	STATE_OBJ_STD: {
		section: null,
		page: null,
		search: null,
		user: null
	},
	SEARCH_PROPERTY_KEY: {encoded:'q', decoded: 'search'},
	
	constructor: function() {
		
	},
	
	decode: function(hashStr) {
		var that = this;
		try {
			var hashStrArr = hashStr.split('/');
			
			/* if we see 2 '/' then the format is like this: /section/page
			*  if we see more than that then it is like this: /user/username/section/page
			*/
			var section = hashStrArr[0];
			var user = null;

			// special case
			if (section == 'user') {
				section = hashStrArr[2];
				user = hashStrArr[1];
				hashStrArr = hashStrArr[3].split(this.PROPERTY_DELIMITER);
			} else {
				hashStrArr = hashStrArr[1].split(this.PROPERTY_DELIMITER);
			}	
	
			var page = + hashStrArr[0];	//typecast to int

			var search = null;
			if(hashStrArr.length > 1){
				//Then we know there is ??? properties
				dojo.forEach(hashStrArr, function(propStr) {
					//TODO just iterate through a list of props...
					var propArr = propStr.split(that.KEY_VALUE_DELIMITER);
					if (propArr[0] == that.SEARCH_PROPERTY_KEY.encoded) {
						search = propArr[1];
					}
				});
			} 
		} catch(err) {
			console.log('Could not decode hash string' + err.message);
			
			return {
				section: 'incoming',
				page: 1,
				search: null,
				user: null
			};
		}
		
		return {
			section: section,
			page: page,
			search: search,
			user: user
		};
	},
	
	encode: function(state) {
		var encodeStr = "";
		for (var prop in state) {
			//undefined is not a reserved keyword
			//if the property exists in the object and its value is truthy
			if (typeof(prop) != 'undefined' && state[prop]) {	
				switch(prop) {
				case 'section':
					if (state[prop] == 'favorites') {
						encodeStr += 'user' + this.STATE_DELIMITER + state['user'] 
									+ this.STATE_DELIMITER + state[prop] + this.STATE_DELIMITER;
					} else {
						encodeStr += state[prop] + this.STATE_DELIMITER;
					}				
					break;
				case 'page':
					encodeStr += state[prop];
					break;
				case this.SEARCH_PROPERTY_KEY.decoded:
					encodeStr += this.PROPERTY_DELIMITER
					+ this.SEARCH_PROPERTY_KEY.encoded
					+ this.KEY_VALUE_DELIMITER
					+ state[prop] ;
					break;
				}
				
			}
		}
		
		return encodeStr;
	},
	
	updateHash: function(newState) {
		/* summary:
		 * 		Takes a new hash state and merges the changes with the current
		 * 		hash state and updates the hash.
		 */
		var curState = this.decode(dojo.hash());
		var mergedState = {};
		
		for (var prop in this.STATE_OBJ_STD){
			// if the property is defined 
			if (typeof(newState[prop]) != 'undefined') {
				mergedState[prop] = newState[prop];
			} else if (curState[prop] != 'undefined'){
				mergedState[prop] = curState[prop];
			} else {
				//the property wasn't defined in the new state nor the current
				//state...
			}
		}
	
		//Add current state properties that are not in the new state		
		dojo.hash(this.encode(mergedState));
	}
	
});
})();
