(function(){
dojo.provide('audioquarium.util.TimePostedProcessor');

dojo.require('dojo.date');
dojo.require('dojo.date.stamp');
dojo.require('dojo.date.locale');

dojo.declare('audioquarium.util.TimePostedProcessor', null, {	
	
	xTimeAgo: null,
	
	constructor: function(postedTime /*string*/) {
		// Unfortunately the time objects we get are not in isoformat so we need to force it 
		postedTime = postedTime.replace(' ', 'T');
		
		var postedDate = dojo.date.stamp.fromISOString(postedTime);
		var currentDate = new Date();
		var xTimeAgo = 'A long time ago';
		
		try {
			var secondsAgo = dojo.date.difference(postedDate, currentDate, 'second');
			var weeksAgo = dojo.date.difference(postedDate, currentDate, 'week');
			var minutesAgo = dojo.date.difference(postedDate, currentDate, 'minute');
			var hoursAgo = dojo.date.difference(postedDate, currentDate, 'hour');
			var daysAgo = dojo.date.difference(postedDate, currentDate, 'day');
			var monthsAgo = dojo.date.difference(postedDate, currentDate, 'month');
			
			//posted a different month more than 1 week ago
			if (monthsAgo && weeksAgo) {
				var postedYear = dojo.date.locale.format(postedDate, {datePattern: 'yyy', selector: 'date'});
				var currentYear = dojo.date.locale.format(currentDate, {datePattern: 'yyy', selector: 'date'});
				if (postedYear != currentYear) {
					xTimeAgo = 'Posted ' + dojo.date.locale.format(postedDate, {selector: 'date', formatLength: 'long'});
				} else {
					// Posted sometime this year
					xTimeAgo = 'Posted ' + dojo.date.locale.format(postedDate, {datePattern: 'MMMM d', selector: 'date'});
				}
			} else if (weeksAgo) {
				xTimeAgo = 'Posted ' + dojo.date.locale.format(postedDate, {datePattern: 'MMMM d', selector: 'date'});
			} else if (daysAgo) {
				var dayOfWeek = dojo.date.locale.format(postedDate, {datePattern: 'EEEE', selector: 'date'});
				var time = dojo.date.locale.format(postedDate, {datePattern: 'hh:mmaa', selector: 'time'});
				xTimeAgo = dayOfWeek + ' at ' + time.replace(' ','').toLowerCase();
			} else if (hoursAgo) {
				var pre = '';
				var post = ' hour ';
				if (minutesAgo % 60 > 30) {
					pre = 'about ';
				}
				if (hoursAgo > 1) {
					post = ' hours '
				}
				xTimeAgo = pre + hoursAgo + post + ' ago'
			} else if (minutesAgo) {
				if (minutesAgo > 1) {
					xTimeAgo = minutesAgo + ' minutes ago'
				} else {
					xTimeAgo = minutesAgo + ' minute ago'
				}
			} else if (secondsAgo) {
				xTimeAgo = secondsAgo + ' seconds ago'
			}
		} catch(err) {
			// do nothing
		}
		
		this.xTimeAgo = xTimeAgo;
	}
	
});
})();