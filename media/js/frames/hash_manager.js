(function(){
dojo.require('dojo.hash');

dojo.require('audioquarium.util.HashHelper');

dojo.ready(function(){
	var curState = null;
	
	dojo.subscribe('/dojo/hashchange', this, publishProperEvent);
		
	function publishProperEvent() {
		var hashHelper = audioquarium.util.HashHelper();
		var newState = hashHelper.decode(dojo.hash());
		
		if (this.curState) {
			if (newState.section != this.curState.section) {
				dojo.publish('section/change', [newState.section]);
			} else if (newState.page != this.curState.page) {
				dojo.publish('page/change', [newState.page]);
			} else if (newState.search != this.curState.search) {
				dojo.publish('query/change', [newState.search]);
			}
		}
		
		this.curState = newState;
	}
});
})();