(function(){

dojo.require("dijit.Dialog");

dojo.require('audioquarium.util.HashHelper');
dojo.require('audioquarium.pages.Listing');
dojo.require('audioquarium.widgets.QuickSearch');
dojo.require('audioquarium.widgets.AccountWidget');

var searchBoxID = 'searchField';

dojo.ready(function(){
	
	// Search barlogin
	var searchBox = new audioquarium.widgets.QuickSearch(searchBoxID);
	
	// Make the initial page
	var pageManager = new audioquarium.pages.Listing();
	
	// Make the nav buttons invoke a hash change by stopping their events before
	// the browser changes the url.
	var navButtons = dojo.query('div#navBar a, div#footer a');
		
	dojo.forEach(navButtons, function(navButton){
		dojo.connect(navButton, 'onclick', navClicked);
	});

	//hack
	dojo.subscribe('account/loggedIn', dojo.hitch(this, function(username) {
		var faveBtn = dojo.query('div#navBar a')[2];
		faveBtn.href = './user/' + username + '/favorites'
	}));
	
	function navClicked(e){
		dojo.stopEvent(e);
		
		var user;
		
		var section = e.target.href.split('/');
		if (section.length > 2) {
			user = section[section.length-2]
		}
		section = section[section.length-1];
		
		
		if (section == audioquarium.pages.Listing.Section.ABOUT) {
			audioquarium.util.HashHelper().updateHash({section: section, page: null});
		} else if (section == audioquarium.pages.Listing.Section.FAVORITES) {
			if (document.loggedIn){
				audioquarium.util.HashHelper().updateHash({section: section, page: 1, user: user});
			} else {
				dojo.publish('account/login');
			}
		} else {
			// make it goto previous page
			audioquarium.util.HashHelper().updateHash({section: section, page: 1});
		}
	}
});
})();
