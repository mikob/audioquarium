(function(){
dojo.provide('audioquarium.widgets.AccountWidget');

dojo.require('dijit._Widget');
dojo.require('dijit._Templated');

dojo.require('audioquarium.widgets.LoginTool');

dojo.declare('audioquarium.widgets.AccountWidget', [ dijit._Widget, dijit._Templated ], {	
	
	// Path to template
	templateString: dojo.cache('audioquarium', 'widgets/templates/AccountWidget.html'),
	//widgetsInTemplate: true,
	
	// Public members
	close: null,		// the function to close the dialog
	
	// Private members
	_connections: null,
	_subscriptions: null,
	_loginDialog: null,
	
	// Dojo attach points
	_profilePic: null,
	_userMsg: null,
	
	constructor: function() {
		this._connections = [];
		this._subscriptions = [];
	},
	
	uninitialize: function() {
		dojo.forEach(this._connections, function(connection){
			dojo.disconnect(connection);
		});
	},
	
	//called after the widget parameters have been set, but before DOM is ready
	postMixInProperties: function() {
	
	},
	
	// Manipulate the DOM when ready
	postCreate: function() {
		//TODO: change to be a local button connection instead of global dojo connect
		//this._connections.push(dojo.connect(this._submitBtn, 'onClick', dojo.hitch(this, this.submit)));
		this.showLoggedOut();
		this._subscriptions.push(dojo.subscribe('account/login', dojo.hitch(this, function(){
			this.login(null, "You must login to do that!");
		})));
	},
	
	showLoggedIn: function(username, accessToken) {
		/* 
		*	summary:		
		* 		
		*	accessToken (optional):
		*		If FB user, need this for accessing other information	
		*/
		var loggedInMsg = document.createElement('p');
		var user = document.createElement('a');
		var logoutBtn = document.createElement('a');

		document.loggedIn = true;
		
		dojo.empty(this._userMsg);
		if (this._loginDialog){
			this._loginDialog.destroyRecursive();
			this._loginDialog = null;
		}
		
		logoutBtn.innerHTML = 'Logout'; 
		logoutBtn.href = './logout/';
		user.innerHTML = username;
		loggedInMsg.innerHTML = 'Welcome ';
		loggedInMsg.appendChild(user);
		loggedInMsg.innerHTML += '. ';
		loggedInMsg.appendChild(logoutBtn);
		
		this._userMsg.appendChild(loggedInMsg);
		
		// show picture
		if (accessToken) {
			dojo.style(this._profilePic, 'visibility', 'visible');
			this._profilePic.src += '&access_token=' + accessToken;
		}
				
		this._connections.push(dojo.connect(logoutBtn, 'onclick', dojo.hitch(this, this.logout)));
	},
	
	showLoggedOut: function() {
		var loginButton = document.createElement('a');

		document.loggedIn = false;		

		dojo.empty(this._userMsg);
		dojo.style(this._profilePic, 'visibility', 'hidden');
		
		loginButton.href = './login/';
		loginButton.innerHTML = "Login";
		
		this._userMsg.appendChild(loginButton);
		
		this._connections.push(dojo.connect(loginButton, 'onclick', dojo.hitch(this, this.login)));
	},
	
	login: function(evt, msg) {

		// Don't let the hyperlink load a new page
		if (evt)
			dojo.stopEvent(evt);

		if (!this._loginDialog) {
			var loginTool = new audioquarium.widgets.LoginTool();
			loginTool.onLogin = dojo.hitch(this, this.showLoggedIn);

			this._loginDialog = new dijit.Dialog({
				title: "Account",
				content: loginTool
			});
			
			if (msg){
				loginTool.showMsg(msg);
			}
			
		}
		
		//show() includes startup
		this._loginDialog.show();
	},
	
	logout: function(evt) {
		var deferred;
		var that = this;
		
		// Don't let the hyperlink load a new page
		dojo.stopEvent(evt);

		//load media from server
		var xhrArgs = {
			url: '/account/logout/',
			handleAs: 'text',
			load: function() {
				that.showLoggedOut();
			}
		}
		
		deferred = dojo.xhrGet(xhrArgs);
	}
	
	
});


})();
