(function(){
dojo.provide('audioquarium.widgets.PostingsList');

dojo.require('dojo.hash');
dojo.require('dijit._Templated');
dojo.require('dijit._Widget');
dojo.require('dojox.collections.ArrayList');

dojo.require('audioquarium.widgets.ThumbnailMediaContainer');
dojo.require('audioquarium.widgets.PageTool');
dojo.require('audioquarium.tools.FilterTool');
dojo.require('audioquarium.util.HashHelper');

var HashHelper = audioquarium.util.HashHelper;

dojo.declare('audioquarium.widgets.PostingsList', [ dijit._Widget, dijit._Templated ], {	
	/* summary:
	*		A widget that shows a page of music
	*/
	//Path to template
	templateString: dojo.cache('audioquarium', 'widgets/templates/PostingsList.html'),
	
	//public
	posts: null,
	listType: null,
	nowPlaying: null,	//keep track of which post is the one in the list being played
	nowFocused: null,	//keep track of which post in the whole list is currently focused
	workingOnNext: null,
	queryObj: null,
	username: null,
	
	//private members
	_page: null,
	_subscriptions: null,
	_toolbar: null,
	
	constructor: function() {
		this.inherited(arguments);
		
		this._subscriptions = [];
		this.posts = new dojox.collections.ArrayList();
		this._toolbar = [];
	},
	
	startup: function() {
		
	},
	
	postCreate: function() {		
		//check the current page in the hash, and set the initial page to the hash-'page' value
		//var hash = dojo.hash()
				
		// we might need to startup the widget on any page, so check the hash to see which page
		//if (hash) {
			//this.hashLook(hash);
		//} else {
			// default change to page 1 if there is no hash
			var hashHelper = HashHelper();
			if (this.username) {
				dojo.hash(hashHelper.encode({section: this.listType, user: this.username, page: 1}));
			} else {
				dojo.hash(hashHelper.encode({section: this.listType, page: 1}));
			}		
			this.changePage(1);
		//}
		this._subscriptions.push(dojo.subscribe('page/change', this, this.changePage));
		this._startToolbar();
	},
	
	changeQuery: function(query) {
		this.queryObj = {q: query};
		
		//unitializeTools
		this._clearTools();
		this._startToolbar();
		this.changePage(1);
	},
	
	_clearTools: function() {
		// each toolbar item must have a destroy method (can be inherited)
		dojo.forEach(this._toolbar, function(tool){
			tool.destroy();
		});
	},
	
	uninitialize: function() {
		dojo.forEach(this._subscriptions, function(handle){
			dojo.unsubscribe(handle);
		});
		
		this._clearTools();
		
		this._clear();
	},
	
	hashLook: function(hash) {
		/* summary:
		 * 		Introspect state by looking at the hash.
		 */
		var hashHelper = HashHelper();
		var hashInfo = hashHelper.decode(hash);
		
		// if we're still on this section
		if (hashInfo.section == this.listType) {
			if (hashInfo.page != this._page) {
				this.changePage(hashInfo.page);
			}
		}
	},
	
	changePage: function(page) {
		// Only changes the page of the current postings list, does not take care
		// of the toolbar. When the user makes a page change, the page changing
		// button toolbar invokes this change
		this._page = page;
		this.populate(page);
	},
	
	_clear: function() {
		/* summary:
		 * 		Remove all the song post items. Preparation for changing the page.
		 */
		this.posts.forEach(function(post){
			post.destroy();
		});
		this.posts.clear();
	},

	populate: function(page) {
		var that = this;
		var deferred;
		
		var url;
		if (this.listType == audioquarium.widgets.PostingsList.ListTypes.FAVORITES) {
			url = '/user/' + this.username + '/favorites/' + page + '/';
		} else {
			url = '/music/' + this.listType + '/' + page + '/';
		}

		//load media from server
		var xhrArgs = {
			url: url,
			content: this.queryObj,
			handleAs: 'json',
			load: function(data) {
				// clear the posts after getting the server data 
				// to make this appear faster
				that._clear();
				that._buildPosts(data);
			},
			error: function(error) {
				console.log('Could not load incoming music from server.');
			}
		}
		deferred = dojo.xhrGet(xhrArgs);
	},
	
	_startToolbar: function() {
		/* summary:
		 * 		Retrieve the total number of pages from server
		 * 		and start the page tool with this info
		 */
		var that = this;
		var deferred;
		
		// Filter tool
		var filterTool = new audioquarium.tools.FilterTool();
		filterTool.placeAt(this.toolbarTag);	// can be placed (2nd arg) at: first, last, before or after
		that._toolbar.push(filterTool);
		
		if (this.listType == audioquarium.widgets.PostingsList.ListTypes.FAVORITES) {
			this.queryObj = {user: this.username};
		}
	
		// Pagination tool
		// load media from server
		var xhrArgs = {
			url: '/music/' + this.listType + '/pages/',
			content: this.queryObj,
			handleAs: 'json',
			load: function(maxPages) {
				var pageToolTop = new audioquarium.widgets.PageTool({
					currentPage: that._page,
					maxPages: maxPages
				});
				pageToolTop.placeAt(that.toolbarTag, "first");
				
				var pageToolBottom = new audioquarium.widgets.PageTool({
					currentPage: that._page,
					maxPages: maxPages
				});
				pageToolBottom.placeAt(that.bottomToolbarTag, "first");
				
				that._toolbar.push(pageToolTop);
				that._toolbar.push(pageToolBottom);
			},
			error: function(error) {
				console.log('Could not load max pages from server.');
			}
		};
		deferred = dojo.xhrGet(xhrArgs);
	},
	
	_buildPosts: function(media) {		
		for (var i = 0; i < media.length; i++) {
			var favorited = false;
			
			// Check if it has been favorited by the user
			if (typeof(media[i].extras) != 'undefined') {
				if (media[i].extras.favorite != 0) {
					favorited = true;
				}
			}
			
			var songPost = new audioquarium.widgets.ThumbnailMediaContainer({
				artistName: media[i].fields.artist,
				songName: media[i].fields.title,
				timeStamp: media[i].fields.time_posted,
				blogComment: '',
				mediaSource: media[i].fields.media_source,
				pk: media[i].pk,
				blogName: '',
				blogs: media[i].fields.blog_sources,
				mediaList: this,
				favorited: favorited
			});

			songPost.placeAt(this.musicListTag, 'last');
			this.posts.add(songPost);
			
			// Restore the song that was previously playing/focused if it
			// is on this page
			if(songPost.pk == dojo.cookie(audioquarium.widgets.PostingsList.CookieTypes.FOCUSED_SONG)) {
				songPost.focus(true);
				if(dojo.cookie(audioquarium.widgets.PostingsList.CookieTypes.PLAYING) == 'true') {
					songPost.togglePlayButton(false);
				}
			}
		}
	},
	
	setPlaying: function(id) {
		this.nowPlaying = id;
		if (id != null) {	//id could be null
			this.workingOnNext = false;
		}
	},
	
	setFocus: function(id /*is mediaSource*/) {
		this.nowFocused = id;
		this.posts.forEach(function(post) {
			if (post.pk != id) {
				post.focus(false);
			} else {
				post.focus(true);
			}
		});
	},

	unfocusAll: function() {
		this.posts.forEach(function(post) {
			post.focus(false);
		});
	},
	
	getNextPosting: function(curID) {
		/* summary:
		 * 		Returns the posting after the current one if it exists. This is
		 * 		useful for getting the next song to play without needing a server
		 * 		call.
		 */
		for (var i = 0; i < this.posts.count; i++) {
			if (this.posts.item(i).pk == curID){
				if (i+1 < this.posts.count) {
					return this.posts.item(i+1)
				}
			}
		}
		
		return null;
	}
	
});

//enumerator of enum list types used for PostingList constructor
audioquarium.widgets.PostingsList.ListTypes = {
	INCOMING: 'incoming',
	POPULAR: 'popular',
	SEARCH: 'search',
	FAVORITES: 'favorites'
};

audioquarium.widgets.PostingsList.CookieTypes = {
	FOCUSED_SONG: 'focused',
	PLAYING: 'playing'
};

})();

