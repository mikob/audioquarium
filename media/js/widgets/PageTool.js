(function() {
dojo.provide('audioquarium.widgets.PageTool');

dojo.require('dijit._Widget');
dojo.require('dijit._Templated');

dojo.require('audioquarium.util.HashHelper');

dojo.declare('audioquarium.widgets.PageTool', [ dijit._Widget, dijit._Templated ], {	
	
	//Path to template
	templateString: dojo.cache('audioquarium', 'widgets/templates/PageTool.html'),
	
	//Constants
	CURRENT_PAGE: 1,
	MAX_PAGES: 99,
	PREVIOUS_PAGE_RANGE: 2,
	NEXT_PAGE_RANGE: 3,
	
	//Widget Properties
	maxPages: null,
	currentPage: null,
	previousPageRange: null,
	nextPageRange: null,
	
	//Private members
	_connections: null,
	_subscriptions: null,
	_currentPageDOMNode: null,	//the currently selected and highlighted page html element
	_buttons: null,
	
	//This maps widget properties (attributes) to where they belong in the widget's dom tree
	attributeMap: {
		thumbnailSource: {
			node: 'thumbnailTag',
			type: 'attribute',
			attribute: 'src'
		}
	},
	
	constructor: function() {
		this._connections = [];
		this._buttons = [];
		this._subscriptions = [];
	},
	
	uninitialize: function() {
		dojo.forEach(this._connections, function(connection){
			dojo.disconnect(connection);
		});
		dojo.forEach(this._subscriptions, function(subscription){
			dojo.unsubscribe(subscription);
		});
	},
	
	//called after the widget parameters have been set, but before DOM is ready
	postMixInProperties: function() {
		this.maxPages = this.maxPages || this.MAX_PAGES;
		this.currentPage = this.currentPage || this.CURRENT_PAGE;
		this.previousPageRange = this.previousPageRange || this.PREVIOUS_PAGE_RANGE;
		this.nextPageRange = this.nextPageRange || this.NEXT_PAGE_RANGE;
	},
	
	// Manipulate the DOM when ready
	postCreate: function() {
		this._makeButtonRange(this.currentPage);
		this._subscriptions.push(dojo.subscribe('page/change', this, 'updatePage'));
	},
	
	updatePage: function(page) {
		/* summary:
		 * 		When the hash is changed, this method is called. It will visually
		 * 		update the toolbar to reflect the new current page.
		 */
		this.currentPage = page;
		this._clear();
		this._makeButtonRange(page);
	},
	
	_changePage: function(page) {
		/* summary:
		 * 		No interior method is called to update the current page; this is
		 * 		because we update the page externally as a subscriber to the updatePage
		 * 		event.
		 */
		//a more sophisticated way to do it (uglier urls though): dojo.objectToQuery(nav)
		audioquarium.util.HashHelper().updateHash({page: page});
	},
	
	_clear: function() {
		dojo.forEach(this._buttons, function(button) {
			dojo.destroy(button);
		});
	},
	
	_makeButtonRange: function(page) {
		//TODO: for performance we can set the innerHTML and hrefs instead
		// of deleting all the buttons and re-creating them
		/* summary:
		 * description:
		 * 		'+' parses to int
		 */
		var pageRange = this.previousPageRange + this.nextPageRange;
		
		var highPageReach = + this.currentPage + this.nextPageRange > this.maxPages ?
				this.maxPages : + this.currentPage + this.nextPageRange;
		
		var lowestPage = highPageReach - pageRange < 1 ?
				 1 : highPageReach - pageRange;
		
		var highestPage = lowestPage + pageRange > this.maxPages ?
				this.maxPages : lowestPage + pageRange;
		
		if (lowestPage > 1) {
			var ellipsis = this._createEllipsis();
			this.pageBarTag.appendChild(ellipsis);
			this._buttons.push(ellipsis);
		}
		
		for (var i = lowestPage; i <= highestPage; i++) {
			var button = this._createPageButton(i);
			this.pageBarTag.appendChild(button);
			
			if (i == this.currentPage) {
				// take care of page number highlighting
				this._updatePageHighlighting(button);
			}
			this._buttons.push(button);
		}
		
		//The ellipsis and next button...
		if (highestPage < this.maxPages) {
			var ellipsis = this._createEllipsis();
			this.pageBarTag.appendChild(ellipsis);
			this._buttons.push(ellipsis);
		}
		
		var prevButton = this._createNavButton(false);
		this.pageBarTag.appendChild(prevButton);
		
		var nextButton = this._createNavButton(true);
		this.pageBarTag.appendChild(nextButton);
		
		this._buttons = this._buttons.concat([prevButton, nextButton]);
	},
	
	_createPageButton: function(num) {
		/* summary:
		 * 		Makes a number button and the button's proper f(x) called when 
		 * 		clicked.
		 */
		var that = this;
		var pageNumButton = document.createElement('a');
		pageNumButton.innerHTML = num;
		
		// set the current page dom selection if this is the current page
		if (this.currentPage == num) {
			this._currentPageDOMNode = pageNumButton;
			dojo.addClass(pageNumButton, 'selected');
		} else {
			// href left in here for useability reasons, but really we will use hashing
			// Find what the href is by using the hash decoder
			var hashObj = audioquarium.util.HashHelper().decode(dojo.hash());
			hashObj.page = num;
			var hrefStr = audioquarium.util.HashHelper().encode(hashObj);
			pageNumButton.href = hrefStr;
			pageNumButton.title = 'Page ' + num;
		}
		
		// when clicked, prevent the url change, instead do a hash change
		dojo.connect(pageNumButton, 'onclick', function(e){
			dojo.stopEvent(e);
			
			// find the section and page number and change the page to that
			var page = + this.innerHTML; // '+' parses to int
			that._changePage(page);
		});
		
		return pageNumButton;
	},
	
	_createEllipsis: function() {
		/* summary:
		 * 		Makes an ellipsis to represent more pages being present
		 */
		var element = document.createElement('a');
		element.innerHTML = '...';
		
		return element;
	},
	
	_createNavButton: function(next) {
		/* summary:
		 * 		Makes a prev/next page nav button
		 * next (bool)
		 * 		make true if it the next button
		 */
		var that = this;
		var navButton = document.createElement('a');
		
		if (next) {
			navButton.innerHTML = '>';
			// href left in here for useability reasons, but really we will use hashing
			navButton.id = 'next';
			navButton.title = 'Next';
			if (this.currentPage < this.maxPages) {
				navButton.href = './incoming/' + ((+ this.currentPage) + 1);
			}
		} else {
			navButton.innerHTML = '<';
			navButton.id = 'previous';
			navButton.title = 'Previous';
			if (this.currentPage > 1) {
				navButton.href = './incoming/' + ((+ this.currentPage) - 1);
			}
		}
		
		// when clicked, prevent the url change, instead do a hash change
		dojo.connect(navButton, 'onclick', function(e){
			dojo.stopEvent(e);
			
			// find the section and page number and change the page to that
			if (this.id == 'next' && that.currentPage < that.maxPages) { 
				that._changePage(that.currentPage + 1);
			} else if (this.id  == 'previous' && that.currentPage > 1) {
				that._changePage(that.currentPage - 1);
			}
		});
		
		return navButton;
	},
	
	_updatePageHighlighting: function(selectedPageDOMNode) {
		//If there was a button previously selected
		if (this._currentPageDOMNode) {
			//make the old selected page a normal button again
			dojo.removeClass(this._currentPageDOMNode, 'selected');
			this._currentPageDOMNode.href = './incoming/' + this._currentPageDOMNode.innerHTML;
		}
		// the new selected page
		dojo.addClass(selectedPageDOMNode, 'selected');
		dojo.removeAttr(selectedPageDOMNode, 'href');
		this._currentPageDOMNode = selectedPageDOMNode;
	}
	
});
})();
