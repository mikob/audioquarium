(function() {
dojo.provide('audioquarium.widgets.ThumbnailMediaContainer');

dojo.require('dojo.hash');
dojo.require('dijit._Widget');
dojo.require('dijit._Templated');

dojo.require('audioquarium.util.TimePostedProcessor');
dojo.require('audioquarium.widgets.AudioManager');
dojo.require('audioquarium.util.HashHelper');

dojo.declare('audioquarium.widgets.ThumbnailMediaContainer', [ dijit._Widget, dijit._Templated ], {	
	
	//Path to template
	templateString: dojo.cache('audioquarium', 'widgets/templates/ThumbnailMediaContainer.html'),
	
	//Static Constants
	THUMBNAIL_FOLDER_LOC: '../../media/music/thumbnails/',
	
	//Widget Properties
	artistName: null,
	songName: null,
	timeStamp: null,		// when the song was last posted by a blog (TODO: show when first posted)
	blogName: null,			// the blog(s) that posted the song
	blogPostLink: null,		
	mediaSource: null,
	pk: null,
	
	//
	nextSong: null,

	//private members
	_connections: null,

	//public members
	mediaList: null,
	blogs: null,
	playing: null,
	favorited: false,
	
	//This maps widget properties (attributes) to where they belong in the widget's dom tree
	attributeMap: {
		artistName: {
			node: 'artistNameTag',
			type: 'innerHTML'
		},
		songName: {
			node: 'songNameTag',
			type: 'innerHTML'
		},
		timeStamp: {
			node: 'timeStampTag',
			type: 'innerHTML'
		},
		thumbnailSource: {
			node: 'thumbnailTag',
			type: 'attribute',
			attribute: 'src'
		}
	},
	
	constructor: function() {
		this._connections = [];
	},
	
	//called after the widget parameters have been set, but before DOM is ready
	postMixInProperties: function() {
		var timePostedProcessor = audioquarium.util.TimePostedProcessor(this.timeStamp);
		this.timeStamp = timePostedProcessor.xTimeAgo;
		this.thumbnailSource = this.THUMBNAIL_FOLDER_LOC + this.pk + '.jpg';	//change to find proper ext
		this.playing = this.playing === null ? false : this.playing;
	},
	
	
	// Manipulate the DOM when ready
	postCreate: function() {		
		// Prepare play button
		this._connections.push(dojo.connect(this.playButtonTag, 'onclick', dojo.hitch(this, this.togglePlayButton)));
		this._connections.push(dojo.connect(this.faveButtonTag, 'onclick', dojo.hitch(this, this.toggleFaveButton)));
		
		// Setup blog links
		var numBlogs = this.blogs.length;
		if (numBlogs > 0) {
			
			if (numBlogs > 1) {
				var blogLink = document.createElement('a');
				blogLink.innerHTML = numBlogs + ' blogs';
				dojo.attr(blogLink, 'href', 'test');
			} else {
				var blogLink = document.createElement('a');
				blogLink.innerHTML = this.blogs[0];
				dojo.attr(blogLink, 'href', 'test');
			}

			this.blogNameTag.innerHTML = 'Posted by ';
			this.blogNameTag.appendChild(blogLink);
			
		}

		// Initialize favorite property
		dojo.toggleClass(this.faveButtonTag, 'favorited', this.favorited);
	},	
	
	uninitialize: function() {
		dojo.forEach(this._connections, function(connection) {
			dojo.disconnect(connection);
		});
	},
	
	togglePlayButton: function(action /*bool*/) {
		/* summary:
		 * 		Toggle's play/pause. Sets visual focus, changes button,
		 * 		and plays/pauses song. If action is false, does not play or
		 * 		pause the song, solely changes visuals (this is used when we 
		 * 		need to recreate a thumbnail that had already been playing) 
		 */
		//styling
		this.mediaList.setFocus(this.pk);
		dojo.toggleClass(this.playButtonTag, 'pause', !this.playing);
		
		//explicitly check for true paying attention to boolean type
		if (!(action === false)) {
			//Play or Pause the music
			if (!this.playing) {
				//start playing
				this.mediaList.setPlaying(this.pk);
				dojo.publish('AudioManager/play', [this]);

				// See if we need to retrieve the next song for consecutive playing
				if (!this.mediaList.workingOnNext && !this.nextSong) {
					this._getNextSong();
				}
			} else {
				//pause
				this.mediaList.setPlaying(null);
				dojo.publish('AudioManager/pause');
			}
		}
		
		this.playing = !this.playing;
		this._updateHashAboutMyState();
	},

	toggleFaveButton: function() {
		/* summary:
		*	Toggle whether the user has this song favorited.
		*/
		var that = this;
		var deferred;

		this.favorited = !this.favorited;
		dojo.toggleClass(this.faveButtonTag, 'favorited', this.favorited);
		
		//load media from server
		var xhrArgs = {
			url: '/account/toggle_favorite/',
			content: {q: this.pk},
			load: function() {
				
			},
			error: function(error) {
				console.log('Could not favorite song on server.');
			}
		}
		deferred = dojo.xhrPost(xhrArgs);
	},
	
	focus: function(focused /*boolean*/) {
		//make this song the one that is visibly being played
		//all other songs should look 'unfocused'
		dojo.toggleClass(this.container, 'focused', focused);
		
		if (!focused && dojo.hasClass(this.playButtonTag, 'pause')) {
			this.playing = false;
			dojo.removeClass(this.playButtonTag, 'pause');
		}
	},	
	
	_updateHashAboutMyState: function() {
		var curState = {};
		
		// if we're updating the hash about one-self, we must be focused
		curState.focused = this.pk;	
		curState.playing = this.playing ? this.pk : curState.playing;
		audioquarium.util.HashHelper().updateHash(curState);
	},
	
	_getNextSong: function() {
		/* summary:
		 * 		Sends out a deferred to get the next songs' info
		 */
		this.mediaList.workingOnNext = true;
		var nextPosting = this.mediaList.getNextPosting(this.pk);
		
		if (nextPosting) {
			this.nextSong = nextPosting;
		} else {
			var that = this;
			var deferred;
	
			//load media from server
			var xhrArgs = {
				url: '/music/' + this.mediaList.listType + '/next_song/',
				content: {q: this.pk},
				handleAs: 'json',
				load: function(media) {
					that.nextSong = new audioquarium.widgets.ThumbnailMediaContainer({
						artistName: media[0].fields.artist,
						songName: media[0].fields.title,
						timeStamp: media[0].fields.time_posted,
						blogComment: '',
						mediaSource: media[0].fields.media_source,
						pk: media[0].pk,
						blogName: '',
						blogs: media[0].fields.blog_sources,
						mediaList: that.mediaList
					});
				},
				error: function(error) {
					console.log('Could not load next song from server.');
				}
			}
			deferred = dojo.xhrGet(xhrArgs);
		}
	}
	
});
})();
