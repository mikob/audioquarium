(function() {
dojo.provide('audioquarium.widgets.QuickSearch');

dojo.require('audioquarium.pages.Listing');
dojo.require('audioquarium.widgets.PostingsList');

dojo.declare('audioquarium.widgets.QuickSearch', null, {
	
	//static
	KEY_DOWN_TIMEOUT: 400,
	
	inputBoxID: null,
	searchPage: null,
	
	//private
	_connections: null,
	
	constructor: function(inputID) {
		this._connections = [];
		this._subscriptions = [];
		this.inputBoxID = inputID;
		
		this._connections.push(dojo.connect(dojo.byId(inputID), 'onkeydown', this, this.keyPressed));		
	},
	
	keyPressed: function(event) {
		switch(event.keyCode){
			//key down
			case 40 : 
				//this.selectPrevious(event); 
				break;
			//key up
			case 38:
				//this.selectNext(event); 
				break;
			//escape
			case 27:
				//this.hide();
				break;
			//anything else
			default:
				this.startTimer();
				break;
		}
	},
	
	startTimer: function() {
		var that = this;
		
		// start timer
		if (this._timer){
			window.clearTimeout(this._timer);
		}
		this._timer = window.setTimeout(function(){
			that.requestSearch();
		}, this.KEY_DOWN_TIMEOUT);
	},
	
	requestSearch: function() {
		var query = dojo.byId(this.inputBoxID).value;
		
		if (query) {
			var searchSection = audioquarium.pages.Listing.Section.SEARCH;
			
			audioquarium.util.HashHelper().updateHash({section: searchSection, search: query});
		}		
	}
	
	
});
})();