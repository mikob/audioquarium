(function(){
dojo.provide('audioquarium.widgets.LoginTool');

dojo.require('dijit._Widget');
dojo.require('dijit._Templated');
dojo.require("dijit.form.Form");
dojo.require("dijit.form.Button");
dojo.require("dijit.form.ValidationTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require('dojox.layout.ContentPane');

dojo.require('audioquarium.util.HashHelper');

dojo.declare('audioquarium.widgets.LoginTool', [ dijit._Widget, dijit._Templated ], {	
	
	// Path to template
	templateString: dojo.cache('audioquarium', 'widgets/templates/LoginTool.html'),
	widgetsInTemplate: true,
	
	// Constants
	INCORRECT_LOGIN_MSG: "Incorrect username/password combination.",
	INVALID_LOGIN_MSG: "Invalid username or password information.",
	SERVER_ERROR_MSG: "Problem communicating with the server.",
	INVALID_FB_LOGIN_MSG: "Error logging in with facebook.",
	FB_SERVER_ERROR: "Facebook server error.",
	
	// Public members
	onLogin: null,		// the function to call upon successful login
	
	// Private members
	_connections: null,
	
	// Dojo attach points
	_username: null,
	_password: null,
	_submitBtn: null,
	_loginDialog: null,
	_statusBar: null,
	_msg: null,
	_fbLoginBtn: null,
	
	constructor: function() {
		this.inherited(arguments);
		this._connections = [];
	},
	
	uninitialize: function() {
		dojo.forEach(this._connections, function(connection){
			dojo.disconnect(connection);
		});
		
		FB.Event.unsubscribe('auth.login', dojo.hitch(this, this.fbLogin));
	},
	
	//called after the widget parameters have been set, but before DOM is ready
	postMixInProperties: function() {
		this.inherited(arguments);
	},
	
	// Manipulate the DOM when ready
	postCreate: function() {
		this.inherited(arguments);
		//TODO: change to be a local button connection instead of global dojo connect
		this._connections.push(dojo.connect(this._fbLoginBtn, 'onclick', dojo.hitch(this, this.fbButtonClick)));
		this._connections.push(dojo.connect(this._loginForm, 'onSubmit', dojo.hitch(this, this.submit)));
		dojo.style(this._statusBar, 'display', 'none');
		
	},
	
	startup: function() {
		this.inherited(arguments);
			
		/*FB.Event.subscribe('auth.sessionchange', function(response) {
			if (response.session) {
				// A user has logged in, and a new cookie has been saved
				alert('logged in');
			} else {
				// The user has logged out, and the cookie has been cleared
				alert('logged out');
			}
		});*/
		
		// TODO: check if fb is ready before parsing.
		FB.XFBML.parse();
		// Dont need this since we get the response elsewhere now
		//FB.Event.subscribe('auth.login', dojo.hitch(this, this.fbLogin));
	},

	fbButtonClick: function(e) {
		dojo.stopEvent(e);

		var that = this;

		FB.login(function(response) {
			that.fbLogin(response);
		}, {perms:'email, offline_access'});
	},
	
	fbLogin: function(response) {
		/*
		 *	summary:
		 *  	First thing that happens after a facebook login
		 */
		 
		var that = this;
		// The user has authorized this application if a session object is present
		if (response.session) {
			that.fbSubmit(response.session.uid, response.session.access_token);
		} else {
			that._msg.innerHTML = that.INVALID_FB_LOGIN_MSG;
			dojo.style(that._statusBar, 'display', 'block');
		}
	},
	
	fbSubmit: function(fbId, accessToken) {	
		/*
		 *  summary:
		 *		Facebook login initiated from login dialog 
		*/	
		var that = this;
		this.loggingIn();
		
		// Call the FB api to get information about the user
		FB.api('/me?fields=first_name,email', function(response){
			
			var userData = {
				fbId: fbId,
				accessToken: accessToken,
				email: response.email
			};
			
			var username = response.first_name;
				
			// authenticate user on the server end
			that.loginToServer(userData, username);
		});
		
	},
	
	submit: function(evt) {
		/*
		 *  summary:
		 *		Submit button pressed from login dialog 
		*/
		
		dojo.stopEvent(evt);
    		if (this._loginForm.validate()) {
			var deferred;
			
			var userData = {
				username: this._username.get('value'),
				password: this._password.get('value')
			};
			
			this.loggingIn();
				
			// authenticate user on the server end
			this.loginToServer(userData);
		} else {
			this._msg.innerHTML = this.INVALID_LOGIN_MSG;
			dojo.style(this._statusBar, 'display', 'block');
		}
     		return false;   
	},
	
	loginToServer: function(userData, username) {
	/*
	 * 	summary:
	 *		Ajax login to server. If facebook data is given, then the server will
	 *  	handle logging in the facebook user if it has logged in before, or 
	 *		it will create a new user to connect to the facebook user if they 
	 *  	haven't logged in before.
	 *	userData (obj):
	 *  	Must have sufficient login credentials. 
	 * 	username [optional] (str):
	 * 		The username to use when logged in
	 */
		var that = this;
		
		var xhrArgs = {
			url: '/account/login/',
			content: userData,
			handleAs: 'text',
			load: function(data) {
				if (data != '0') {
					that.loggedIn();
					if (username){
						that.onLogin(username, userData.accessToken);
						dojo.publish('account/loggedIn', [data]);
					} else {
						that.onLogin(data);
					}				
				} else {
					that.showMsg(that.INCORRECT_LOGIN_MSG);
				}
			},
			error: function(error) {
				that.showMsg(that.SERVER_ERROR_MSG);
			}
		}
		deferred = dojo.xhrPost(xhrArgs);
	},
	
	getFbUserData: function(fields, fbId, accessToken) {
		/*
		 *  summary:
		 *		Get data using access_token and FB oauth 
		 *	fields:
		 *  	What data to request from facebook. Check graph api for 
		 *		what can be requested. 
		*/
		var that = this;
		
		var query = {
			access_token: accessToken,
			fields: fields.toString()
		};
						
		var xhrArgs = {
			url: 'https://graph.facebook.com/' + fbId,
			content: query,
			handleAs: 'json',
			error: function(error) {
				that.showMsg(that.FB_SERVER_ERROR);
			}
		}
		
		// return a deferred
		return dojo.xhrGet(xhrArgs);
	},
	
	showMsg: function(msg) {
		this._submitBtn.disabled = false;
		this._submitBtn.innerHTML = "OK";
		
		this._msg.innerHTML = msg;
		dojo.style(this._statusBar, 'display', 'block');
	},

	loggingIn: function() {
		this._submitBtn.disabled = true;
		this._submitBtn.innerHTML = 'Logging in...';

		dojo.style(this._statusBar, 'display', 'none');
	},

	loggedIn: function() {
		// summary: what to do when log in was succesful
		
	}
	
	
});


})();
