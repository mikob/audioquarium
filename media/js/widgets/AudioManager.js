dojo.provide('audioquarium.widgets.AudioManager');

dojo.require('dijit._Widget');
dojo.require('dijit._Templated');
dojo.require('dojo.cookie');

dojo.require('audioquarium.widgets.PostingsList');

dojo.declare('audioquarium.widgets.AudioManager', [ dijit._Widget, dijit._Templated ], {
	//Path to template
	templateString: dojo.cache('audioquarium', 'widgets/templates/AudioManager.html'),
	
	//baseCachedMediaSource: 'http://127.0.0.1:8000/media/music/',

	_subscriptions: null,
	
	nowPlaying: null,
	activeMediaList: null,
	
	constructor: function() {
		this._subscriptions = [];
	},
	
	//Manipulate the DOM when ready
	postCreate: function() {
		// we need proper scope for the play f(x)
		this._subscriptions.push(dojo.subscribe('AudioManager/play', this, 'play'));
		this._subscriptions.push(dojo.subscribe('AudioManager/pause', this, 'pause'));
		
		// when the site is first loaded we always want the songs to be paused...
		this.savePaused();
	},	

	uninitialize: function() {
		dojo.forEach(this._subscriptions, function(handle){
			dojo.unsubscribe(handle);
		});
	},
	
	play: function(mediaContainer) {	
		var that = this;
		
		//set the now-playing cookie
		this.saveWhatsPlaying(mediaContainer.pk);		

		//check if already playing
		if (this.nowPlaying && (this.nowPlaying.sID == mediaContainer.pk)) {
			//resume
			this.nowPlaying.resume();
		} else {
			//create & play the new media object
			if (this.nowPlaying) {
				//play a different song
				this.nowPlaying.destruct();
			}
			
			this.nowPlaying = soundManager.createSound({
				id: mediaContainer.pk,		//unique
				url: /*this.baseMediaSource +*/ mediaContainer.mediaSource
			});
			
			this.nowPlaying.play({
				onfinish: function() { 
					if (that.nowPlaying) {
						that.nowPlaying.destruct();
					}
					
					that.playNext(mediaContainer);
				}
			});
		}
	},
		
	pause: function() {
		soundManager.pauseAll();
		this.savePaused();
	},
	
	playNext: function(mediaContainer) {		
		var nextSong = mediaContainer.nextSong;
		
		if (nextSong) {
			nextSong.togglePlayButton();
		} else {
			mediaContainer.togglePlayButton();
		}
	},

	saveWhatsPlaying: function(songPK) { 
		/* Summary:
		*	Tell the server what's currently playing so it can give us back
		*	a cookie.	
		*/
		dojo.cookie(audioquarium.widgets.PostingsList.CookieTypes.FOCUSED_SONG, songPK);
		dojo.cookie(audioquarium.widgets.PostingsList.CookieTypes.PLAYING, true);
	},

	savePaused: function() {
		/* Summary:
		*	Simply tell the server that the song was paused by the user so that
		*	it knows the song wasn't only focused.
		*/
		dojo.cookie(audioquarium.widgets.PostingsList.CookieTypes.PLAYING, false);
	}	
	
});
