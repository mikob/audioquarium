(function() {
dojo.provide('audioquarium.tools.FilterTool');

dojo.require('dijit._Widget');
dojo.require('dijit._Templated');

dojo.require('audioquarium.util.HashHelper');

dojo.declare('audioquarium.tools.FilterTool', [ dijit._Widget, dijit._Templated ], {	
	
	//Path to template
	templateString: dojo.cache('audioquarium', 'tools/templates/FilterTool.html'),
	
	//Constants
	
	//Widget Properties
	
	//Private members
	_connections: null,
	_subscriptions: null,
	_buttons: null,
	
	//This maps widget properties (attributes) to where they belong in the widget's dom tree
	attributeMap: {
		/*thumbnailSource: {
			node: 'thumbnailTag',
			type: 'attribute',
			attribute: 'src'
		}*/
	},
	
	constructor: function() {
		this._connections = [];
		this._buttons = [];
		this._subscriptions = [];
	},
	
	uninitialize: function() {
		dojo.forEach(this._connections, function(connection){
			dojo.disconnect(connection);
		});
		dojo.forEach(this._subscriptions, function(subscription){
			dojo.unsubscribe(subscription);
		});
	},
	
	//called after the widget parameters have been set, but before DOM is ready
	postMixInProperties: function() {
		
	},
	
	// Manipulate the DOM when ready
	postCreate: function() {
		// Default first element is selected
		var allButton = dojo.query('a[ value=\"all\"]', this.domNode)[0];
		dojo.toggleClass(allButton, 'selected', true);
	}
	
	
});
})();
