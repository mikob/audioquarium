(function(){
dojo.provide('audioquarium.pages.Listing');

dojo.require('dojo.hash');

dojo.require('audioquarium.widgets.PostingsList');
dojo.require('audioquarium.pages.About');
dojo.require('audioquarium.util.HashHelper');

var HashHelper = audioquarium.util.HashHelper;

dojo.declare('audioquarium.pages.Listing', null, {
	
	// constants
	PAGE_CONTENT_DIV: 'pageContent',
	
	// public
	contents: null,	//whats inside the page
	
	// private
	_subscriptions: null,
	
	constructor: function() {
		this.contents = [];
		this._subscriptions = [];
		dojo.subscribe('section/change', this, this.makePage);
		
		//create initial page
		this.makePage(audioquarium.pages.Listing.Section.INCOMING_MUSIC);
	},

	makePage: function(pageType){
		this.clearPage();
		
		switch(pageType) {
		case audioquarium.pages.Listing.Section.ABOUT:
			var aboutPage = new audioquarium.pages.About().placeAt(this.PAGE_CONTENT_DIV);
			
			this.contents.push(aboutPage);
			break;
		case audioquarium.pages.Listing.Section.SEARCH:		
			var searchType = audioquarium.widgets.PostingsList.ListTypes.SEARCH;
			var hashObj = audioquarium.util.HashHelper().decode(dojo.hash());
			var queryObj = {q: hashObj.search};
			
			var searchPage = new audioquarium.widgets.PostingsList({
				listType: searchType,
				queryObj: queryObj
			}).placeAt(this.PAGE_CONTENT_DIV);
			searchPage.startup();
			
			//TODO MOVE INTO NEW WIDGET
			this._subscriptions.push(dojo.subscribe('query/change', searchPage, 'changeQuery'));
			
			this.contents.push(searchPage);
			break;
		case audioquarium.pages.Listing.Section.FAVORITES:
			var favoriteType = audioquarium.widgets.PostingsList.ListTypes.FAVORITES;
			var hashObj = audioquarium.util.HashHelper().decode(dojo.hash());
			
			var favoritePage = new audioquarium.widgets.PostingsList({
				listType: favoriteType,
				username: hashObj.user
			}).placeAt(this.PAGE_CONTENT_DIV);
			favoritePage.startup();

			this.contents.push(favoritePage);
			break;
		default:
			var incomingType = audioquarium.widgets.PostingsList.ListTypes.INCOMING;
			
			var postsList = new audioquarium.widgets.PostingsList({
				listType: incomingType
			}).placeAt(this.PAGE_CONTENT_DIV);
			postsList.startup();
			
			this.contents.push(postsList);
			break;
		}
	},
	
	clearPage: function(){
		dojo.forEach(this.contents, function(content){
			content.destroy();
		});
		
		dojo.forEach(this._subscriptions, function(subscription) {
			dojo.unsubscribe(subscription);
		});
		
		// will this suffice for complete destruction of all the postings?
		this.contents = [];
	}
	
	
});

audioquarium.pages.Listing.Section = {
	ABOUT: 'about',
	SEARCH: 'search',
	INCOMING_MUSIC: 'incoming',
	POPULAR_MUSIC: 'popular',
	FAVORITES: 'favorites'
};

})();
