(function(){
dojo.provide('audioquarium.pages.About');

dojo.require('dijit._Templated');
dojo.require('dijit._Widget');

dojo.declare('audioquarium.pages.About', [ dijit._Widget, dijit._Templated ], {	
	
	//Path to template
	templateString: dojo.cache('audioquarium', 'pages/templates/About.html'),
	
	constructor: function() {
		this.inherited(arguments);
	},
	
	postCreate: function() {
		this.inherited(arguments);
	},
	
	startup: function() {
		this.inherited(arguments);
	}
	
});
})();